package guy.droid.com.dropboxapi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.icu.text.UnicodeSetSpanner;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.core.DbxWebAuth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MainActivity extends AppCompatActivity {
    final static private String APP_KEY = "---";
    final static private String APP_SECRET = "---";
    private int PICK_IMAGE_REQUEST = 1;
    private DropboxAPI<AndroidAuthSession> mDBApi;
    String accessToken = "";
    String fileName,extension;
    File file;
    Button create;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);
        mDBApi.getSession().startOAuth2Authentication(MainActivity.this);
        create = (Button)findViewById(R.id.createfile);
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");


        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                   // creatFile();
                    new LongOperation().execute();
                }catch (Exception e)
                {
                    Toast.makeText(MainActivity.this, "ER:::"+e.toString(), Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDBApi.getSession().authenticationSuccessful()) {
            try {
                // Required to complete auth, sets the access token on the session
                mDBApi.getSession().finishAuthentication();

                accessToken = mDBApi.getSession().getOAuth2AccessToken();

            } catch (IllegalStateException e) {
                Log.i("DbAuthLog", "Error authenticating", e);
                Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
            }
        }else
        {
            //Toast.makeText(getApplicationContext(),"AUTH FAIL",Toast.LENGTH_LONG).show();
        }
    }
    public void ViewToken(View view)
    {
        Toast.makeText(getApplicationContext(),accessToken,Toast.LENGTH_LONG).show();
    }
    public  void creatFile() throws Exception
    {
     //   File file = new File("working-draft.txt");
        FileInputStream inputStream = new FileInputStream(file);
        DropboxAPI.Entry response = mDBApi.putFile("/uploaded."+extension, inputStream,
                file.length(), null, null);
        Log.i("DbExampleLog", "The uploaded file's rev is: " + response.rev);
    }
    public void FileChooser(View view)
    {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();


            try {
                file = new File(SelectedFilePath.getPath(getApplicationContext(),filePath));
                fileName = file.getName();
                extension = fileName.substring(fileName.lastIndexOf("."));

            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),"ERROR "+e.getMessage()+"\n"+e.getCause(),Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }


        }
    }
    class LongOperation extends AsyncTask<String,Integer,String>
    {

        @Override
        protected String doInBackground(String... params) {
            try {
                FileInputStream inputStream = new FileInputStream(file);
                DropboxAPI.Entry response = mDBApi.putFile("/uploads/"+fileName, inputStream,
                        file.length(), null, null);
                Log.i("DbExampleLog", "The uploaded file's rev is: " + response.rev);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (DropboxException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(progressDialog.isShowing())
            {
                progressDialog.cancel();
            }


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    }


}
